module Staticizable


=begin
	# актуализирован, если актуализация не отложена и не истек срок актуализации
	# отложена, если есть update_later
	# change
	# change2
	# change3 (no ok)
=end

	extend ActiveSupport::Concern
	included do
		class << self;
			attr_accessor :has_child_custom
			#after_initialize :default_stat_values

		end
	end

	module ClassMethods
		def check_children child
			self.has_child_custom = child
		end
	end


	#do we need to staticize block?
	def need_update?
		return true unless staticized_date
		# if self.staticize_later
		# 	(self.staticize_later.to_datetime - DateTime.now).to_f <= 0
		# else

		period = self.staticize_period || 30
		if period < 0
			perion = -30
		end
		(DateTime.now - staticized_date).to_i > period

		# end 1 1 2
	end

	def staticized?
		if self.staticize_later
			return false
		end

		!need_update?
	end


	def staticized_date
		self[:staticized_date] ? self[:staticized_date].to_datetime : nil #DateTime.new(2000, 1, 1, 1, 1, 1, 1, 1)
	end


	def update_in_time(value, unit)
		period = value.send(unit.pluralize.to_sym)
		self.staticize_later = DateTime.now+period
		@statisized_in_time = true
		self.save!
		recalc_object

	end

	def staticize
		self.staticize_later = nil
		self.staticized_date = DateTime.now
		@just_staticized = true
		self.save!
		recalc_object
	end

	def recalc_object
		self.building.recalc_act_block_percentage if self.building
	end
=begin


	def was_staticized
		unless staticized_date
			return false
		end
		(DateTime.now - self.staticized_date.to_datetime).to_i
	end

	def days_from_staticized
		unless self.staticized_date
			return "не проводилась"
		end

		#"#{(DateTime.now - self.staticized_date.to_datetime).to_i} дней назад"
	end

	def staticized?
		if self.class.has_child_custom.present?
			@unstatisized_blocks = []
			id = self.id
			Block.select(%w'id staticized_date staticize_period staticize_later').where { building_id == id }.each do |b|
				unless b.staticized?
					@unstatisized_blocks << b.id
				end
			end

			if @unstatisized_blocks.length > 0
				return false
			else
				return true
			end

		else
			if self.staticize_later.present?
				if self.staticize_period > (DateTime.now - self.staticize_later.to_datetime).to_i
					true
				else
					false
				end
			else
				was_staticized_int = (was_staticized ? 1 : 0)
				if self.staticize_period > was_staticized_int
					true
				else
					false
				end
			end

		end

	end


	def unstaticized_blocks
		@unstatisized_blocks
	end

	def staticize in_time_params=nil

		if self.class.has_child_custom.present?
			if self.staticized?
				self.staticize_later = nil
				self.staticized_date = DateTime.now
				save
				return true
			else
				return @unstatisized_blocks
			end
		else
			if in_time_params.present?
				self.staticize_later = case in_time_params[:unit]
												when 'hour'
													DateTime.now + in_time_params[:amount].to_i.hour
												when 'day'
													DateTime.now + in_time_params[:amount].to_i.day
												when 'month'
													DateTime.now + in_time_params[:amount].to_i.month
												else
													nil
											end
				save
			else
				self.staticized_date = DateTime.now
				save
			end

		end

	end
=end

end
